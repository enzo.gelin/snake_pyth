import pygame
from pygame.locals import *
import sys, time, random, os
from datetime import datetime

#Initialisation de pygame et vérification qu'il fonctionne correctement
check_errors = pygame.init()
# Regarde si la deuxieme valeur de 'check_errors' est égale à 0, si elle l'est cela veut dire qu'il n'y a pas d'erreurs.
if check_errors[1] == 0:
    print('Initialisé avec succès')
else:
    # On quitte python et ferme la page
    sys.exit('Attention : Initialisé sans succès, la page va se fermer ...')

# Diverses couleurs
red=(255, 0, 0)
green=(56, 242, 56)
yellowgreen=(154, 205, 50)
greenlawn = (52, 201, 36)
blue=(0, 0, 255)
yellow=(255, 255, 0)
white=(255, 255, 255)
black=(0, 0, 0)
gray=(50, 50, 50)
greensnake = (43, 138, 68)
violet = (134, 121, 156)

# Centrage de l'application sur la fenêtre
os.environ['SDL_VIDEO_CENTERED'] = '1'

HIGH_SCORE_FILE = 'scores.txt'

# Resolution du jeu (meme taille pour la longueur et la largeur facilite le code)
window_screen_width = 800
window_screen_height = 800
window_game = pygame.display.set_mode((window_screen_width, window_screen_height))

# Nom de la fenêtre
pygame.display.set_caption('Snakezer')

# Nombre de frame par secondes 
frame_per_second = 10 # nombre d'image par seconde ( plus le chiffre de FPS est bas , plus le jeu sera lent et simple)
frame_per_second_clock = pygame.time.Clock()

# Couleur du fond de la fenetre de jeu
background = pygame.Surface(window_game.get_size())
background = background.convert()
background.fill(white)

# Police d'écriture
font_retro = "Retro.ttf"
font_snake= "SnakeChan.ttf"
font_dpcomic = "dpcomic.ttf"

# Jeu divers option et Variables du jeu
rows = 20
size_ratio = window_screen_width // rows # ratio longueur divisé par nombre de lignes
grid = [[False for i in range(rows)] for j in range(rows)] # Grille de jeu
snake = [[5, 10, 0, 1],[5, 9, 0, 1],[5, 8, 0, 1],[5, 7, 0, 1],[5, 6, 0, 1],[5, 5, 0, 1],[5, 4, 0, 1],[5, 3, 0, 1], [5, 2, 0, 1], [5, 1, 0, 1]] # position du serpent dans la grille (taille : 10 cases)
# snake = [[5, 3, 0, 1], [5, 2, 0, 1], [5, 1, 0, 1]] # position du serpent dans la grille (taille : 3 cases)
direction = (0, 1) # Direction vers le bas
apple = (random.randint(0, 19), random.randint(0, 19), 0, 0) # position des pommes sur la grille

# Dessin de la grille sur la fenetre
def draw_grid():
    window_game.fill(green)
    x, y = 0, 0
    for i in range(rows):
        x += size_ratio # ajout à X le ratio
        y += size_ratio # ajout à Y le ratio
        pygame.draw.line(window_game, yellowgreen, (x, 0), (x, window_screen_width)) # dessin des lignes verticales
        pygame.draw.line(window_game, yellowgreen, (0, y), (window_screen_width, y)) # dessin des lignes horizontales

def spawn_apple():
    global snake, apple # Permet l'utilisation des vars (snake et apple) defini hors de la fonction
    x, y = random.randint(0, 19), random.randint(0, 19) # Défini une position aléatoire à la pomme quand elle va apparaitre
    while len(list(filter(lambda c: c[0] == x and c[1] == y, snake))) > 0:
        x, y = random.randint(0, 19), random.randint(0, 19)
    apple = (x, y, 0, 0) # Position finale de la pomme

def draw_apple():
    global apple # Permet l'utilisation de la var (apple) defini hors de la fonction
    draw_circle(apple, color='randomiser')

def eat_apple():
    global snake, apple # Permet l'utilisation des vars (snake et apple) defini hors de la fonction
    x,y,_,_ = snake[0] # Position de la tete du serpent
    x2, y2, _, _ = apple # Position de la pomme

    if x == x2 and y == y2: # Verification si la pomme a était manger ou non en verifiant les position du serpent et de la pomme
        spawn_apple() # Générer une nouvelle pomme
        x, y, dx, dy = snake[-1] # augmentation de la taille du serpent
        snake.append([x + (dx*-1), y + (dy*-1), dx, dy]) # augmentation de la taille du serpent

def draw_cube(pos, color=greensnake, eyes=False):
    RGB = (random.randint(33,53),random.randint(128,148),random.randint(58,78))
    global size_ratio # Permet l'utilisation de la var (size_ratio) defini hors de la fonction
    x, y,_,_ = pos # Defini les vars selon la position envoyé
    pygame.draw.rect(window_game, RGB, (x * size_ratio + 1, y * size_ratio + 1, size_ratio - 1, size_ratio - 1)) # Dessine le carré dans la grille
    if eyes:
        centre = size_ratio // 2
        radius = 3
        circle_middle = (x * size_ratio + centre - radius, y * size_ratio + 8)
        circle_middle_2 = (x * size_ratio + centre - radius * 4, y * size_ratio + 8)
        pygame.draw.circle(window_game, white, circle_middle, radius)
        pygame.draw.circle(window_game, white, circle_middle_2, radius)

def draw_circle(pos, color=black):
    global size_ratio # Permet l'utilisation de la var (size_ratio) defini hors de la fonction
    x, y,_,_ = pos # Defini les vars selon la position envoyé
    centre = size_ratio // 2
    radius = 19
    if color == 'randomiser':
        R = random.randint(200,255)
        color=(R, 0, 0)
    pygame.draw.circle(window_game, color, (x * size_ratio + centre, y * size_ratio + 20),radius) # Dessine le carré dans la grille

def draw_snake():
    for i in range(len(snake)):
        cube = snake[i]
        draw_cube(cube, eyes=i==0)

def move_snake():
    global direction, rows, snake
    change_direction()
    set_cube_direction()
    for cube in snake:
        x, y, dx, dy = cube
        cube[0] += dx
        cube[1] += dy

        if cube[0] > rows - 1 and dx > 0:
            cube[0] = 0
        elif cube[0] < 0 and dx < 0:
            cube[0] = rows - 1
        elif cube[1] > rows - 1 and dy > 0:
            cube[1] = 0
        elif cube[1] < 0 and dy < 0:
            cube[1] = rows - 1
        
    if len(list(filter(lambda c: c[0] == snake[0][0] and c[1] == snake[0][1], snake[1:]))) > 0:
        high_score = get_high_score() # Récupération du meilleur score actuel
        current_score = len(snake)
        save_score(current_score) # Sauvegarde du score effectué durant la partie
        best_score = False
        if current_score > high_score: # Comparaison des deux valeurs pour determiner si le meilleur score est battu
            best_score = True
            save_high_score(current_score) # Sauvegarde du score effectué durant la partie entant que Meilleur score
        print('Tu as mangé ta queue, quelle vie ... SCORE :', len(snake))
        final_screen(len(snake),best_score) #Affichage de la page finale avec deux params (score, si cest un meiller score)
        
def change_direction():
    global direction, snake
    for event in pygame.event.get():
        x, y,_,_ = snake[0]

        if event.type == pygame.QUIT:
            pygame.quit()
        keys = pygame.key.get_pressed()
        cd = direction

        if keys[pygame.K_LEFT]: # Virage a gauche
            direction = (-1, 0)
        elif keys[pygame.K_RIGHT]: # Virage a droite
            direction = (1, 0)
        elif keys[pygame.K_UP]: # En haut
            direction = (0, -1)
        elif keys[pygame.K_DOWN]: # En bas
            direction = (0, 1)
        elif keys[pygame.K_ESCAPE]: # Echap (quitte le jeu)
            quit_game()
        
        if direction[0] * -1 == cd[0] and direction[1] * -1 == cd[1]:
            direction = cd

def set_cube_direction():
    global snake, direction
    dxh, dyh = None, None
    for i in range(len(snake)):
        x, y, dx, dy = snake[i]
        snake[i] = [x, y, dxh, dyh]
        dxh, dyh = dx, dy
    snake[0][2] = direction[0]
    snake[0][3] = direction[1]   

# Fonction rendu du texte parametres : (message, police, taille, couleur)
def text_format(message, textFont, textSize, textColor):
    newFont=pygame.font.Font(textFont, textSize) #Type de font + taille de police
    newText=newFont.render(message, 0, textColor) # Contenu + Couleur police
    return newText

# Menu principal
def main_home():

    home_status=True
    selected="start"
    while home_status:
        for event in pygame.event.get():
            # Liste des evenement du menu
            if event.type==pygame.QUIT:
                quit_game()
            if event.type==pygame.KEYDOWN: # Action des fleches directrices sur le menu
                if event.key==pygame.K_UP:
                    selected="start"
                elif event.key==pygame.K_DOWN:
                    selected="option"
                elif event.key==pygame.K_ESCAPE:
                    selected="quit"
                if event.key==pygame.K_RETURN: # Validation du choix avec Entrée
                    if selected=="start": # Commence le jeu
                        game()
                    if selected=="option": # Commence le jeu
                        option_menu()
                    if selected=="quit":
                        quit_game()

        # Menu et titre de la page d'accueil
        window_game.fill(greenlawn)
        title = text_format("Snake'zer", font_snake, 90, yellow)

        # Bouton commencer et effet hover
        if selected=="start":
            text_start = text_format("Commencer", font_dpcomic, 75, white)
        else:
            text_start = text_format("Commencer", font_dpcomic, 75, black)

        # Bouton option et effet hover !!!!!!!!!!!!!!!!!!! EDIT OPTION À Scores
        if selected=="option":
            text_option = text_format("Scores", font_dpcomic, 75, white)
        else:
            text_option = text_format("Scores", font_dpcomic, 75, black)
        
        # Bouton quitter et effet hover
        if selected=="quit":
            text_quit = text_format("Quitter le jeu (press ESC)", font_dpcomic, 75, white)
        else:
            text_quit = text_format("Quitter le jeu (press ESC)", font_dpcomic, 75, black)

        hs = get_high_score()
        best_score = text_format("Meilleur score : " + str(hs), font_dpcomic, 60, yellow)

        # création des 3 rectangles couvrants les surfaces titre, commencer, quitter
        title_rect=title.get_rect()
        start_rect=text_start.get_rect()
        option_rect=text_option.get_rect()
        quit_rect=text_quit.get_rect()
        best_score_rect=best_score.get_rect()

        # Liaison des rectangles dans la page
        window_game.blit(title, (window_screen_width/2 - (title_rect[2]/2), 80))
        window_game.blit(text_start, (window_screen_width/2 - (start_rect[2]/2), 250))
        window_game.blit(text_option, (window_screen_width/2 - (option_rect[2]/2), 310))
        window_game.blit(text_quit, (window_screen_width/2 - (quit_rect[2]/2), 370))
        window_game.blit(best_score, (window_screen_width/2 - (best_score_rect[2]/2), 570))

        # Mise à jour de la page
        pygame.display.update()
        frame_per_second_clock.tick(frame_per_second)

# Quitte le jeu
def quit_game():
    pygame.quit() 
    quit()

# Menu option
def option_menu():
    option_status=True
    while option_status:
        for event in pygame.event.get():
            # Liste des evenement du menu
            if event.type==pygame.QUIT:
                quit_game()
            if event.type==pygame.KEYDOWN: # Action des fleches directrices sur le menu
                if event.key==pygame.K_ESCAPE:
                    main_home()

        # Menu et titre de la page d'accueil
        window_game.fill(greenlawn)
        title = text_format("Scores", font_retro, 90, yellow)

        # création des 3 rectangles couvrants les surfaces titre, commencer, quitter
        title_rect=title.get_rect()

        # Liaison des rectangles dans la page
        window_game.blit(title, (window_screen_width/2 - (title_rect[2]/2), 80))
        
        # Récuperation des scores 
        scores_list = get_scores()
        scores_array = scores_list.split()
        # Creation du tableau visuel
        x = 0
        for val in scores_array:
            val_array = val.split("|")
            score_ = text_format(val_array[1] + "  score : " +val_array[0], font_retro, 60, yellow)
            score__rect=score_.get_rect()
            window_game.blit(score_, (window_screen_width/2 - (score__rect[2]/2), 150 + x))
            x += 50
        
        # Mise à jour de la page
        pygame.display.update()
        frame_per_second_clock.tick(frame_per_second)

# Ecran Final
def final_screen(score,best):
    option_status=True
    while option_status:
        for event in pygame.event.get():
            # Liste des evenement du menu
            if event.type==pygame.QUIT:
                quit_game()
            if event.type==pygame.KEYDOWN: # Action des fleches directrices sur le menu
                if event.key==pygame.K_ESCAPE:
                    main_home()

        # Menu et titre de la page d'accueil
        window_game.fill(greenlawn)
        title = text_format("Partie terminée", font_retro, 90, yellow)
        score_final = text_format("votre score : " + str(score), font_retro, 70, black)
        if(best == True):
            best_sc = text_format("!!! Nouveau meilleur score !!! ", font_dpcomic, 50, red)
        

        # création des 3 rectangles couvrants les surfaces titre, commencer, quitter
        title_rect=title.get_rect()
        score_final_rect=score_final.get_rect()
        if(best == True):
            best_sc_rect=best_sc.get_rect()
        

        # Liaison des rectangles dans la page
        window_game.blit(title, (window_screen_width/2 - (title_rect[2]/2), 80))
        window_game.blit(score_final, (window_screen_width/2 - (score_final_rect[2]/2), 200))
        if(best == True):
            window_game.blit(best_sc, (window_screen_width/2 - (best_sc_rect[2]/2), 450))  
        

        # Mise à jour de la page
        pygame.display.update()
        frame_per_second_clock.tick(frame_per_second)

# Jeu SNAKE
def game():
    game_status=True
    while game_status:
        move_snake()
        eat_apple()
        draw_grid()
        draw_snake()
        draw_apple()
        pygame.time.delay(50)
        frame_per_second_clock.tick(frame_per_second)
        pygame.display.update()

# Récupérer le score dans le ficheir texte
def get_high_score():
    # Valeur par defaut de high_score
    high_score = 0
    # Essayez de lire le meilleur score à partir du fichier high_score
    try:
        high_score_file = open("high_score.txt", "r")
        high_score = int(high_score_file.read())
        high_score_file.close()
        # print("The high score is", high_score)
    except IOError:
        # Erreur lors de la lecture du fichier, pas de meilleur score
        print("Il n'y a pas encore de meilleur score.")
    except ValueError:
        # Erreur avec le fichier et ce qu'il contient
        print("Erreur avec le fichier high_score. Merci de le supprimer.")
 
    return high_score

# Récupérer les score dans le ficheir texte
def get_scores():
    # Valeur par defaut de score
    scores = ""
    # Essayez de lire le meilleur score à partir du fichier high_score
    try:
        scores_file = open("scores.txt", "r")
        scores = str(scores_file.read())
        scores_file.close()
        # print("The high score is", high_score)
    except IOError:
        # Erreur lors de la lecture du fichier, pas de meilleur score
        print("Il n'y a pas encore de score.")
    except ValueError:
        # Erreur avec le fichier et ce qu'il contient
        print("Erreur avec le fichier score. Merci de le vider.")
 
    return scores

# Stocker le meilleur score max dans le ficheir texte
def save_high_score(new_high_score):
    try:
        # Ecrit sur le fichier high_score
        high_score_file = open("high_score.txt", "w")
        high_score_file.write(str(new_high_score))
        high_score_file.close()
    except IOError:
        # Erreur ecriture.
        print("Sauvegarde du meilleur score impossible.")

# Stocker le score max dans le ficheir texte
def save_score(new_score):
    try:
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y_%H:%M:%S")
        # Essayez de lire les scores à partir du fichier scores
        save_score_file = open("scores.txt", "r")
        save_score = save_score_file.read()
        save_score_file.close()
        # Écrit sur le fichier scores
        score_file = open("scores.txt", "w")
        score_file.write(str(save_score) + '\n' + str(new_score)+'|'+str(dt_string))
        score_file.close()
    except IOError:
        # Erreur ecriture.
        print("Sauvegarde du score impossible.")

# Initialisation du jeu
main_home() # Lance la fonction home
pygame.quit()
quit()